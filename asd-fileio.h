#pragma once
/*
* Author: AndrewDunetz
* Copyright SuperSetInteractive LLC 2020-2021
*/

#include "asd-alloc.h"
#include "asd-types.h"

typedef struct asd_FileWriteResult
{
	int valid;
	asd_Error errorCode;

	size_t writeSizeBytes;
} asd_FileWriteResult;

typedef struct asd_FileReadResult
{
	int valid;
	asd_Error errorCode;

	byte *data;
	size_t dataSizeBytes;
} asd_FileReadResult;

asd_FileReadResult asd_ReadEntireFile(asd_Allocator allocator, char *filepath);
asd_FileWriteResult asd_WriteEntireFile(asd_Allocator allocator, char *filepath);

#ifdef IMPLEMENTATION
#ifdef _WIN32

#ifndef asd_WIN32
	#define asd_WIN32
	#define WIN32_LEAN_AND_MEAN
	#include "windows.h"
#endif // asd_WIN32

#include "asd-error.h"

asd_FileReadResult asd_ReadEntireFile(asd_Allocator allocator, char *filepath)
{
	HANDLE file = CreateFile( filePath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

	if (file == INVALID_FILE_HANDLE)
	{
		return (asd_FileReadResult)
		{
			.valid = false,
			.errorCode = asd_MarkError(GetLastError()),
			.data = 0,
			.dataSizeBytes = 0,
		};
	}

	DWORD filesizeHigh = 0;
	DWORD fileSize = GetFileSize(file, &fileSizeHigh);

	if (fileSizeHigh)
	{
		return (asd_FileReadResult)
		{
			.valid = false,
			.errorCode = asd_ErrorFileTooBig,
			.data = 0,
			.dataSizeBytes = 0,
		};
	}

	byte *data = allocator.alloc(fileSize, allocator.markup);
	DWORD bytesRead = 0;
	int readOkay = ReadFile(file, data, fileSize, &bytesRead, 0);
	
	if (!readOkay)
	{
		allocator.free(data);
		return (asd_FileReadResult)
		{
			.valid = false,
			.errorCode = asd_MarkError(GetLastError()),
			.data = 0,
			.dataSizeBytes = 0,
		};
	}

	CloseHandle(file);

	return (asd_FileReadResult)
	{
		.valid = true,
		.errorCode = 0,
		.data = data,
		.dataSizeBytes = bytesRead,
	};
}

asd_FileWriteResult asd_WriteEntireFile(asd_Allocator allocator, char *filepath, void *data, size_t dataSizeBytes)
{
	HANDLE file = CreateFile( filePath, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

	if (file == INVALID_FILE_HANDLE)
	{
		return (asd_FileWriteResult)
		{
			.valid = false,
			.errorCode = asd_MarkError(GetLastError()),
			.writeSizeBytes = 0,
		};
	}

	DWORD bytesWritten = 0;
	int writeOkay = WriteFile(file, data, fileSize, &bytesWritten, 0);
	
	if (!writeOkay)
	{
		return (asd_FileWriteResult)
		{
			.valid = false,
			.errorCode = asd_MarkError(GetLastError()),
			.writeSizeBytes = bytesWritten,
		};
	}

	CloseHandle(file);

	return (asd_FileWriteResult)
	{
		.valid = true,
		.errorCode = 0,
		.writeSizeBytes = bytesWritten,
	};
}

#endif // _WIN32
#endif // IMPLEMENTATION
