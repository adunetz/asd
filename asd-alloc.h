/*
* Author: AndrewDunetz
* Copyright SuperSetInteractive LLC 2020-2021
*/
#pragma once

/// Allocation Markup Type
#ifndef asd_MemoryMarkup
	typedef struct asd_MemoryMarkup
	{
		int nop;
	} asd_MemoryMarkup;
#endif

/// Utility Type Declarations
typedef struct asd_MemBlock
{
	void *mem;
	size_t sizeBytes;
} asd_MemBlock;

/// Annotated Memory Function Types
typedef void *Alloc(size_t, asd_MemoryMarkup);
typedef void Free(void *);
typedef void *Realloc(void *, size_t, asd_MemoryMarkup);

/// Allocator Type
typedef struct asd_Allocator
{
	Alloc *alloc;
	Free *free;
	Realloc *realloc;
	asd_MemoryMarkup markup;
} asd_Allocator;

/// Function Prototypes
	asd_Allocator asd_AllocatorCreate(Alloc *alloc, Free *free, Realloc *realloc, asd_MemoryMarkup markup);
	asd_Allocator asd_AllocatorMarkup(asd_MemoryMarkup markup);
	asd_Allocator asd_AllocatorClone(asd_Allocator allocator, asd_MemoryMarkup markup);

/// Instantiated Global Variables
extern asd_Allocator asd_defaultAllocator;

#ifdef IMPLEMENTATION
#ifndef asd_malloc
	#include "stdlib.h"

/// default allocator calls if asd_malloc undef
void *asd_malloc(size_t size, asd_MemoryMarkup markup)
{ return malloc(size); }

void asd_free(void *ptr)
{ free(ptr); }

void *asd_realloc(void *ptr, size_t size, asd_MemoryMarkup markup)
{ return realloc(ptr, size); }

#endif // asd_malloc

/// Function Definitions
asd_Allocator asd_defaultAllocator = {asd_malloc, asd_free, asd_realloc, (asd_MemoryMarkup){0}};

asd_Allocator asd_AllocatorCreate(Alloc *alloc, Free *free, Realloc *realloc, asd_MemoryMarkup markup)
{ return (asd_Allocator) { alloc, free, realloc, markup}; }

asd_Allocator asd_AllocatorClone(asd_Allocator allocator, asd_MemoryMarkup markup)
{ return (asd_Allocator) {allocator.alloc, allocator.free, allocator.realloc, markup}; }

asd_Allocator asd_AllocatorMarkup(asd_MemoryMarkup markup)
{ return (asd_Allocator) {asd_malloc, asd_free, asd_realloc, markup}; }

#else
#endif // IMPLEMENTATION






