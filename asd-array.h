/*
 * Author: Andrew Dunetz
 * Copyright SuperSet Interactive LLC 2020-2021
*/
#pragma once

#include "asd-alloc.h"

#ifndef memmove
	#include "string.h"
#endif

typedef struct ArrayHeader
{
	size_t count;
	size_t cap;
	size_t itemSize;
	size_t growFactor;
	asd_Allocator allocator;
} ArrayHeader;

#define Array(type) type

#define ARRAY_ADD(a,i) do\
	{\
	size_t index = ArrayReserve((void*)&(a), 1);\
	a[index] = i;\
} while(0)

#define ARRAY_COPY(array, dst, src, itemSize) do\
{ \
	char *arC = array;\
	size_t dstC = (dst)*itemSize;\
	size_t srcC = (src)*itemSize;\
	for (size_t i = 0; i < itemSize; i++)\
	{ arC[dstC + i] = arC[srcC + i]; }\
} while (0)

#define ARRAY_CONTAINS(array, count, item)\
	({int found = 0; for (unsigned int macro_i = 0; macro_i < count; macro_i++)  { if (array[macro_i] == item) {found = 1; break;} }; found; })

#ifndef ARRAY_NO_PROTOTYPES
	ArrayHeader *ArrayGetHeader(void* array);
	size_t ArrayCount(void* array);
	size_t ArrayCap(void* array);
	void* HeaderGetArray(ArrayHeader *header);
	void* ArrayCreateEx(asd_Allocator allocator, size_t itemSize, size_t initialItemCap);
	void* ArrayCreate(size_t itemSize, size_t initialItemCap);
	void ArrayDestroy(void* array);
	void ArrayRemove(void * array, size_t remIndex);
	size_t ArrayReserve(void ** array, size_t reserveCount);
	void* ArrayShrinkToFit(void* array);
	char *ArrayConcatCStrings(char *a, char *b);
#endif //ARRAY_NO_PROTOTYPES

#ifdef IMPLEMENTATION
void *asd_ArrayMovePointerAddress(void *ptr, int diff)
{ return (void*)(((char*)ptr) + diff); }

ArrayHeader *ArrayGetHeader(void* array)
{ return (ArrayHeader*)array - 1; }

size_t ArrayCount(void* array)
{ return ArrayGetHeader(array)->count; }

size_t ArrayCap(void* array)
{ return ArrayGetHeader(array)->cap; }

void* HeaderGetArray(ArrayHeader *header)
{ return header + 1; }

void* ArrayCreate(size_t itemSize, size_t initialItemCap)
{ return ArrayCreateEx(asd_defaultAllocator, itemSize, initialItemCap); }

void* ArrayCreateEx(asd_Allocator allocator, size_t itemSize, size_t initialItemCap)
{
	size_t headerSize = ((sizeof(ArrayHeader)/itemSize)+1)*itemSize;
	size_t headerDiff = headerSize - sizeof(ArrayHeader);

	ArrayHeader *header = (ArrayHeader*) allocator.alloc( itemSize*initialItemCap+headerSize, allocator.markup); 

	header = asd_ArrayMovePointerAddress(header, headerDiff);

	*header = (ArrayHeader){
		.count = 0,
		.cap = initialItemCap,
		.itemSize = itemSize,
		.growFactor = 4,
		.allocator = allocator
	};
	//LogFormat("Array Create: %p(%p)", header, header+1);

	return header + 1;
}

void ArrayDestroy(void* array)
{
	ArrayHeader *header = ArrayGetHeader(array);
	size_t headerSize = ((sizeof(ArrayHeader)/header->itemSize)+1)*header->itemSize;
	size_t headerDiff = headerSize - sizeof(ArrayHeader);
	header->allocator.free(asd_ArrayMovePointerAddress(header, -headerDiff));
}

void ArrayRemove(void* array, size_t remIndex)
{
	//LogFormat("ArrayRemove");
	ArrayHeader *header = ArrayGetHeader(array);
	for (size_t i = remIndex; i < header->count-1; i++)
	{ ARRAY_COPY(array, i, i+1, header->itemSize); }
	header->count--;
}

size_t ArrayReserve(void ** array, size_t reserveCount)
{
	ArrayHeader *header = ArrayGetHeader(*array);
	size_t avail = header->cap - header->count;

	if (avail < reserveCount)
	{
		//LogFormat("ArrayReserve Realloc");
		size_t newArrayCap = header->cap;
		if (newArrayCap == 0)
		{ newArrayCap = header->growFactor; }

		while (newArrayCap < header->count + reserveCount)
		{ 
			// TODO(robustness): OVERFLOW vulnerability
			newArrayCap *= header->growFactor; 
		}
		size_t headerSize = ((sizeof(ArrayHeader)/header->itemSize)+1)*header->itemSize;
		size_t headerDiff = headerSize - sizeof(ArrayHeader);
		size_t newArraySize = headerSize + header->itemSize*newArrayCap;

		void *np = header->allocator.realloc( asd_ArrayMovePointerAddress(*array, -headerSize), newArraySize, header->allocator.markup);
		header = asd_ArrayMovePointerAddress(np, headerDiff);
		header->cap = newArrayCap;
		//LogFormat("\t new array address: %p", header);
	}

	size_t index = header->count;
	header->count += reserveCount;

	*array = HeaderGetArray(header);

	return index;
}

void* ArrayShrinkToFit(void* array)
{
	ArrayHeader *header = ArrayGetHeader(array);

	if (header->count == header->cap) 
	{return array;}
	
	size_t tempCount = header->count;

	void* result = ArrayCreateEx(header->allocator, header->itemSize, header->count);
	memmove(result, array, header->itemSize*header->count);
	*ArrayGetHeader(result) = *ArrayGetHeader(array);

	ArrayGetHeader(result)->cap = tempCount;

	ArrayDestroy(array);

	return result;
}

#endif //IMPLEMENTATION
