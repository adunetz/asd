/*
* Author: AndrewDunetz
* Copyright SuperSetInteractive LLC 2020-2021
*/

#pragma once

#include "asd-alloc.h"
#include "asd-array.h"

/// string source
typedef struct strs
{
	char *source; // const array
	int len;
	asd_Allocator allocator;
} strs;

/// string build
typedef struct strb
{
	char *a_data; 
	asd_Allocator allocator;
} strb;

/// string view
typedef struct strv
{
	strs *source;
	int start;
	int len;
} strv;

typedef struct SearchResult
{
	int valid;
	int item;
} SearchResult;

/// string builder functions
/* returns a string builder (strb), a mutable, modifiable, string */
strb strbCreate();
strb strbCreateEx(asd_Allocator allocator);
/* clones the content of a string view (strv) into a string builder for modification */
strb strbFrom(strv view);
strb strbFromEx(asd_Allocator allocator, strv view);
/* adds the content of the string view 'addition' onto the end of string builder target */
strb strbConcat(strb target, strv addition);
/* removes the contents of the string builder at loc and until loc+len */
strb strbRemove(strb target, int loc, int len);
/* removes the contents as above, and then replaces them with the content in replacement */
strb strbReplace(strb target, int loc, int len, strv replacement);
/* reverses the string as in abcde -> edcba */
strb strbReverse(strb target);
/* converts letters a->z to A->Z */
strb strbToUpper(strb target);
/* converts letters A->Z to a->z */
strb strbToLower(strb target);
/* inserts the contents of 'insert' into string builder 'target' at loc */
strb strbInsert(strb target, int loc, strv insert);
/* creates a string source from the string builder 'build' and returns a string view of that entire source. */
strv strbView(strb build);
strv strbViewEx(asd_Allocator allocator, strb build);

/// string view functions
/* uses 'allocator' to create a new string source from 'target' and returns a string view of that entire derived source. */
strv strvCloneSource(asd_Allocator allocator, strv target);
/* creates a string source from the cstring literal */
strv strvC(char *literal);
strv strvCEx(asd_Allocator allocator, char *literal);
/* allocate and return an array of string views of target separated by delim */
strv *strSplit(strv target, strv delim);/**returns array*/
/* return a substring at loc in target */
strv strvSubstring(strv target, int loc, int len);
/* return < 0 if (a < b), > 0 if (a > b), or 0 if (a == b)  */
int strvCmp(strv a, strv b);
/* returns 1 if a==b otherwise returns 0 */
int strvEq(strv a, strv b);
/* allocate and return an array of string views of searchString in target */
strv *StrOccurances(strv target, strv searchString);/**returns array*/
/* return the first occurance of searchString in target, or {0,0} if there is none */
SearchResult strvFind(strv target, strv searchString);
/* dealloc the string source (strs) pointed to by view */
void strvDestroySource(strv view);
/* allocated a new null terminated cstring and copy the contents of view into it. */
char *cstring(strv view);
char *cstringEx(asd_Allocator allocator, strv view);
/* fetch the index-th character of target */
char strvAt(strv target, int index);

/// implementation
#ifdef IMPLEMENTATION

/// tested: no
strb strbCreate()
{
	return strbCreateEx(asd_defaultAllocator);
}

/// tested: no
strb strbCreateEx(asd_Allocator allocator)
{
	strb builder = (strb) { .a_data = ArrayCreateEx(allocator, sizeof(char), 32), .allocator = allocator};
	return builder;
}

/// tested: no
strb strbFrom(strv view)
{
	return strbFromEx(asd_defaultAllocator, view);
}

/// tested: no
strb strbFromEx(asd_Allocator allocator, strv view)
{
	strb builder = strbCreateEx(allocator);
	builder = strbConcat(builder, view);
	return builder;
}

/// tested: no
strb strbConcat(strb target, strv addition)
{
	int dest = ArrayReserve((void**)&target.a_data, addition.len);
	for (int i = 0; i < addition.len; i++)
	{
		target.a_data[dest + i] = strvAt(addition, i);
	}
	return target;
}

/// tested: no
strb strbRemove(strb target, int loc, int len)
{
	int source = loc + len;
	int dest = loc;

	int maxLen = ArrayCount(target.a_data) - loc;
	len = maxLen < len ? maxLen : len;

	int i = 0;
	for (i = 0; i < len && source < ArrayCount(target.a_data); i++)
	{
		target.a_data[loc + i] = target.a_data[source++];
	}

	for (/*do nothing*/; i < len; i++)
	{
		target.a_data[loc + i] = 0;
	}

	return target;
}

/// tested: no
strb strbReplace(strb target, int loc, int len, strv replacement)
{
	target = strbRemove(target, loc, len);
	target = strbInsert(target, loc, replacement);
	return target;
}
/// tested: no
strb strbReverse(strb target)
{
	for (int i = 0; i < ArrayCount(target.a_data)/2; i++)
	{
		char c = target.a_data[i];
		target.a_data[i] = target.a_data[(ArrayCount(target.a_data)-1)-i];
		target.a_data[ArrayCount(target.a_data)-i] = c;
	}
	return target;
}

/// tested: no
strb strbToUpper(strb target)
{
	int diff = 'a' - 'A';
	for (int i = 0; i < ArrayCount(target.a_data); i++)
	{
		char c = target.a_data[i];
		if (c >= 'a' && c <= 'z')
		{
			target.a_data[i] -= diff;
		}
	}
	return target;
}

/// tested: no
strb strbToLower(strb target)
{
	int diff = 'a' - 'A';
	for (int i = 0; i < ArrayCount(target.a_data); i++)
	{
		char c = target.a_data[i];
		if (c >= 'A' && c <= 'Z')
		{
			target.a_data[i] += diff;
		}
	}
	return target;
}

/// tested: no
strb strbInsert(strb target, int loc, strv insert)
{
	int moveCount = ArrayCount(target.a_data)-loc;
	int insertDest = ArrayReserve((void**)target.a_data, insert.len);


	int moveDest = ArrayCount(target.a_data)-1;
	/// copy the current data that would be overwritten to a higher position
	for (int i = 0; i < moveCount; i++)
	{
		target.a_data[moveDest-i] = target.a_data[(moveDest-i) - insert.len];  
	}

	/// copy in the new data
	for (int i = 0; i < insert.len; i++)
	{
		target.a_data[insertDest + i] = insert.source->source[insert.start + i];
	}
	return target;
}

/// tested: no
strv strbView(strb build)
{
	return strbViewEx(asd_defaultAllocator, build);
}

void strbDestroy(strb build)
{
	build.allocator.free(build.a_data);
}

char strvAt(strv target, int index)
{
	return *(target.source->source + target.start + index);
}

/// tested: no
strv strbViewEx(asd_Allocator allocator, strb build)
{
	strs *source = allocator.alloc(sizeof(strs), allocator.markup);
	
	*source = (strs) {
		allocator.alloc(ArrayCount(build.a_data)+1, allocator.markup), 
		ArrayCount(build.a_data)+1,
		allocator
	};

	for (int i = 0; i < ArrayCount(build.a_data); i++)
	{
		source->source[i] = build.a_data[i];
	}

	/// null terminate for fallback safety
	source->source[ArrayCount(build.a_data)] = 0;

	strv view = (strv) 
	{
		.source = source,
		.start = 0,
		.len = ArrayCount(build.a_data),
	};

	return view;
}

/// tested: no
strv strvCloneSource(asd_Allocator allocator, strv target)
{
	strs *source = allocator.alloc(target.len+1, allocator.markup);
	*source = (strs) {
		allocator.alloc(target.len, allocator.markup), 
		target.len,
		allocator
	};

	for (int i = 0; i < target.len; i++)
	{
		source->source[i] = strvAt(target,i);
	}

	/// NOTE: manual null termination should be unnecessary because this is copying another source,
	///  which should already be null terminated. Even so, we'll force it to be null terminated 
	///  anyway.

	source->source[target.len] = 0;

	strv view = (strv) 
	{
		.source = source,
		.start = 0,
		.len = target.len,
	};

	return view;
}

/// tested: no
strv strvC(char *literal)
{ return strvCEx(asd_defaultAllocator, literal); }

/// tested: no
strv strvCEx(asd_Allocator allocator, char *literal)
{
	int len = 0;
	for (len = 0; literal[len] && len < (1024*1024); len++) 
	{ /* do nothing - loop increment is main goal */}
	
	/// adjust for null terminator
	len++;

	strs *source = allocator.alloc(sizeof(strs), allocator.markup);
	*source = (strs) {
		allocator.alloc(len, allocator.markup), 
		len-1,
		allocator
	};

	for (int i = 0; i < (len-1); i++)
	{
		source->source[i] = literal[i];
	}

	/// NOTE: manual null termination should be unnecessary because this is copying another source,
	///  which should already be null terminated. Even so, we'll force it to be null terminated 
	///  anyway.

	source->source[len-1] = 0;

	strv view = (strv) 
	{
		.source = source,
		.start = 0,
		.len = len-1,
	};

	return view;
}

/// tested: no
strv *strSplit(strv target, strv delim)/**returns array*/
{
	strv *splits = ArrayCreate(sizeof(strv), 8);

	strv view = strvSubstring(target, 0, target.len);
	SearchResult result = strvFind(view, delim);

	if (result.valid)
	{ ARRAY_ADD(splits, strvSubstring(target, view.start, result.item)); }

	while (result.valid)
	{
		view = strvSubstring(target, result.item + delim.len, view.len);
		result = strvFind(view, delim);
		if (result.valid)
		{ ARRAY_ADD(splits, strvSubstring(target, view.start, result.item)); }
	}

	/// add final split
	if (view.start < target.len)
	{
		ARRAY_ADD(splits, strvSubstring(target, view.start, 100));
	}

	return splits;
}

/// tested: no
strv strvSubstring(strv target, int loc, int len)
{
	int maxLen = target.len - loc;
	len = maxLen < len ? maxLen : len;
	strv view = (strv) { .source = target.source, .start = target.start + loc, .len = len };
	return view;
}

/// tested: no
int strvCmp(strv a, strv b)
{
	int t = 0;

	int len = a.len < b.len ? a.len : b.len;

	char ca = strvAt(a, t);
	char cb = strvAt(b, t);

	while (t < len && ca == cb)
	{
		t++;
		ca = strvAt(a, t);
		cb = strvAt(b, t);
	}

	if (ca == cb)
	{
		return a.len - b.len;
	}

	return ca - cb;
}

/// tested: no
int strvEq(strv a, strv b)
{
	return !strvCmp(a,b);
}

/// tested: no
/**returns array*/strv *StrOccurances(strv target, strv searchString)
{
	strv *occurances = ArrayCreate(sizeof(strv), 8);

	strv view = strvSubstring(target, 0, target.len);
	SearchResult result = strvFind(view, searchString);
	if (result.valid)
	{ ARRAY_ADD(occurances, view); }

	while (result.valid)
	{
		view = strvSubstring(target, result.item, searchString.len);
		SearchResult result = strvFind(view, searchString);
		if (result.valid)
		{ ARRAY_ADD(occurances, view); }
	}

	return occurances;
}

/// tested: no
SearchResult strvFind(strv target, strv searchString)
{
	for (int i = 0; i < target.len - searchString.len; i++)
	{
		int found = 1;
		for (int j = 0; j < searchString.len; j++)
		{
			if (target.source->source[target.start+i+j] == searchString.source->source[searchString.start+j])
			{
				/*do nothing*/
			}
			else
			{
				found = 0;
				break;
			}
		}
		if (found)
		{
			return (SearchResult) {.valid = 1, .item = i};
		}
	}
	return (SearchResult) {.valid = 0, .item = 0};
}

/// tested: no
void strvDestroySource(strv view)
{ 
	asd_Allocator allocator = view.source->allocator;
	allocator.free(view.source->source); 
	allocator.free(view.source);
}

/// tested: no
char *cstring(strv target)
{ return cstringEx(asd_defaultAllocator, target); }

/// tested: no
char *cstringEx(asd_Allocator allocator, strv target)
{
	int len = target.len + 1;
	char *cstr = allocator.alloc(len,allocator.markup);
	for (int i = 0; i < (len-1); i++)
	{
		cstr[i] = strvAt(target,i);
	}
	cstr[len-1] = 0;
	return cstr;
}

#endif // IMPLEMENTATION
