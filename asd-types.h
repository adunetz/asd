#pragma once
/*
* Author: AndrewDunetz
* Copyright SuperSetInteractive LLC 2020-2021
*/
#include <stdint.h>
#include <stdbool.h>
#include <float.h>

typedef int8_t i8;
typedef uint8_t u8; 
typedef int16_t i16; 
typedef uint16_t u16; 
typedef int32_t i32; 
typedef uint32_t u32; 
typedef int64_t i64; 
typedef uint64_t u64; 
typedef uint8_t byte;
typedef float f32;
typedef double f64;
/// compatibility
typedef float r32;
typedef double r64;

typedef i32 b32;

#ifdef __clang__
#define asd_NativeVectors

typedef float vec2 __attribute__((ext_vector_type(2)));
typedef float vec3 __attribute__((ext_vector_type(3)));
typedef float vec4 __attribute__((ext_vector_type(4)));
typedef vec2 Range;

typedef int ivec2 __attribute__((ext_vector_type(2)));
typedef int ivec3 __attribute__((ext_vector_type(3)));
typedef int ivec4 __attribute__((ext_vector_type(4)));

#endif // __clang__

#define T_MIN(a) _Generic((a),\
	i8: INT8_MIN,\
	u8: 0,\
	i16: INT16_MIN,\
	u16: 0,\
	i32: INT32_MIN,\
	u32: 0,\
	i64: INT64_MIN,\
	u64: 0,\
	float: FLT_MIN,\
	double: DBL_MIN)

#define T_MAX(a) _Generic((a),\
	i8: INT8_MAX,\
	u8: UINT8_MAX,\
	i16: INT16_MAX,\
	u16: UINT16_MAX,\
	i32: INT32_MAX,\
	u32: UINT32_MAX,\
	i64: INT64_MAX,\
	u64: UINT64_MAX,\
	float: FLT_MAX,\
	double: DBL_MAX)
