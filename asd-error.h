#pragma once
/*
* Author: AndrewDunetz
* Copyright SuperSetInteractive LLC 2020-2021
*/

asd_Error asd_MarkError(u32 in_error);
u32 asd_UnMarkError(asd_Error in_error);
char *asd_ErrorGetString(asd_Error in_error);

#ifdef IMPLEMENTATION
#ifdef _WIN32

#include "asd-types.h"

#ifndef asd_WIN32
	#define asd_WIN32
	#define WIN32_LEAN_AND_MEAN
	#include "windows.h"
#endif // asd_WIN32

asd_Error asd_MarkError(u32 in_error)
{
	asd_Error error = in_error;
	error |= 0x80000000;
	return error;
}

u32 asd_UnMarkError(asd_Error in_error)
{
	u32 error = in_error;
	return error;
}

char *asd_ErrorGetString(asd_Error in_error)
{
	int externalError = (0x80000000 & in_error);
	if (externalError)
	{
		in_error = (0x7fffffff & in_error);
		DWORD error = in_error;
		if (error == 0)
		{ return asd_errorStrings[0]; }
	
		LPSTR messageBuffer = 0; 
			/// operating system allocation
			/// this could be freed with LocalFree()
			/// NOTE: NOT FREED
		size_t messageSizeBytes = FormatMessageA(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			0, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, 0
		);
	
		return messageBuffer;
	}
	else
	{
		return asd_errorStrings[in_error];
	}
}
	
#endif // _WIN32
#endif // IMPLEMENTATION
