#pragma once
/*
* Author: AndrewDunetz
* Copyright SuperSetInteractive LLC 2020-2021
*/
#include "asd-alloc.h"
#include "asd-array.h"
//#include "asd-string.h"

#ifdef asd_ARCHIVEVERSION
	#undef asd_ARCHIVEVERSION
#endif // asd_ARCHIVEVERSION
#define asd_ARCHIVEVERSION 1

#ifndef asd_archiveTempString
	asd_Allocator asd_archiveTempString = (asd_Allocator) {asd_malloc, asd_free, asd_realloc, (asd_MemoryMarkup){0}};
#endif // asd_ARCHIVETEMPSTRING

typedef struct Archive Archive;

typedef struct ArchiveFileHeader
{
	size_t version;
	size_t headerSizeBytes;

	size_t labelOffset;
	size_t labelSizeBytes;

	size_t valueTableOffset;
	size_t valueTableSizeBytes;

	size_t dataHeapOffset;
	size_t dataSizeBytes;
} ArchiveFileHeader;

typedef union ArchiveNestInterp
{
	ArchiveFileHeader file;
	Archive *link;
} ArchiveNestInterp;

typedef enum ArchiveValueType
{
	Archive_Bad = 0,
	Archive_Int = 1,
	Archive_Float = 2,
	Archive_String = 3,
	Archive_Binary = 4,
	Archive_Nested = 5, // ptr to Archive (in memory) offset of another ArchiveFileHeader (on file)
	Archive_Count,
} ArchiveValueType;

typedef struct ArchiveValue
{
	ArchiveValueType type;

	size_t labelOffset;
	size_t labelSizeBytes;

	/// if type == nested, offset is index into archive.nested array.
	/// else offset is the offset in bytes to the start of the data in archive.dataHeap
	size_t offset;
	size_t sizeBytes;
} ArchiveValue;

struct Archive
{
	asd_Allocator allocator;
	char *label; /** string */

	ArchiveValue *values; /** array */
	unsigned char *dataHeap; /** array */
	Archive *nested; /** array */
};

typedef struct ArchiveReadResult
{
	int valid;
	Archive item;
} ArchiveReadResult;

extern char *archiveValueTypeLabels[Archive_Count];

#ifdef IMPLEMENTATION
#include "asd-fileio.h"
char *archiveValueTypeLabels[Archive_Count] =
{
	"Bad",
	"Int",
	"Float",
	"String",
	"Binary",
	"NestedArchive",
};

char asd_ArchiveSentinel[] = "Quire Archive Sentinel";

/// tested: no
ArchiveReadResult ArchiveReadBad(asd_Error error)
{
	char *string = asd_ErrorGetString(error);
	asd_print(strvC(string), asd_printEnd);
	return (ArchiveReadResult)
	{
		.valid = false,
		.item = {0},
	};
}

/// tested: no
void *asd_ArchiveMovePointerAddress(void *ptr, int diff)
{ return (void*)(((char*)ptr) + diff); }

/// tested: no
Archive ArchiveCreate(asd_Allocator allocator, char *label)
{
	int labelSize = strlen(label);

	Archive archive = (Archive)
	{
		.values =  ArrayCreateEx(allocator, sizeof(ArchiveValue), 64),
		.dataHeap =  ArrayCreateEx(allocator, sizeof(unsigned char), 1024),
		.label = allocator.alloc(labelSize, allocator.markup),
		.nested = ArrayCreateEx(allocator, sizeof(Archive), 8),
		.allocator = allocator,
	};

	for (int i = 0; i < labelSize; i++)
	{ archive.label[i] = label[i]; }

	return archive;
}

#if 0
/// tested: no
ArchiveReadResult ArchiveReadFile(asd_Allocator allocator, asd_Allocator returnAllocator, char *fileName)
{
	asd_FileData file = asd_ReadEntireFile(allocator, fileName);
	if (file.valid)
	{
		return ArchiveReadMemory(allocator, returnAllocator, file.mem, 1);
	}
	else
	{
		return ArchiveReadBad();
	}
}
/// tested: no
int ArchiveReadVersionFromMemory(void *mem)
{
	char *file = mem;
	for (int i = 0; i < sizeof(asd_ArchiveSentinel); i++)
	{
		if (file[i] != asd_ArchiveSentinel[i])
		{
			return 0;
		}
	}
	ArchiveFileHeader *header = mem;
	return header->version;
}
/// tested: no
ArchiveReadResult ArchiveReadMemory(asd_Allocator allocator, asd_Allocator returnAllocator, void *mem, int performVersionCheck)
{
	if (performVersionCheck)
	{
		int version = ArchiveReadVersionFromMemory(mem);

		if (!version)
		{ return ArchiveReadBad(); }

		if (version != asd_ARCHIVEVERSION)
		{ return ArchiveReadBad(); }
	}

	ArchiveFileHeader *header = asd_ArrayMovePointerAddress(mem, sizeof(asd_ArchiveSentinel));

	if (header->headerSizeBytes != sizeof(ArchiveFileHeader))
	{ return ArchiveReadBad(); }

	void *dataHeap = asd_ArchiveMovePointerAddress(mem, header->dataHeapOffset);
	size_t dataHeapSize = header->dataHeapSize;

	char *label = asd_ArchiveMovePointerAddress(mem, header->labelOffset);
	size_t labelSizeBytes = header->labelSizeBytes;

	ArchiveValue *values = asd_ArchiveMovePointerAddress(mem, header->valueTableOffset);
	size_t valueTableSizeBytes = header->valueTableSizeBytes;
	size_t valueTableEntryCount = valueTableSizeBytes/sizeof(ArchiveValue);

	size_t valueTableBytesLeftOver = valueTableSizeBytes%sizeof(ArchiveValue);
	if (valueTableBytesLeftOver)
	{ return ArchiveReadBad(); }

	Archive archive = ArchiveCreateEx(allocator, returnAllocator, label);
	int offset = ArrayReserve((void**)&archive.dataHeap, dataHeapSize);
	for (size_t i = 0; i < dataHeapSize; i++)
	{ archive.dataHeap[i] = ((char*)dataHeap)[i]; }

	for (int i = 0; i < valueTableEntryCount; i++)
	{
		ArchiveValue *value = values[i];
		ARRAY_ADD(archive.values, *value);
	}

	return archive;
}
/// tested: no
asd_FileWriteResult ArchiveWriteFile(Archive archive, char *label, char *fileName)
{
	asd_MemBlock block = ArchiveWrite(archive, label, asd_defaultAllocator);
	asd_FileWriteResult result = asd_WriteEntireFile(block, fileName);
	asd_defaultAllocator.free(block.mem);
	return result;
}
/// tested: no
asd_MemBlock ArchiveWrite( Archive archive, char *label, asd_Allocator allocator)
{
	size_t labelSizeBytes = strlen(label);
	
	size_t memorySize = 
		sizeof(asd_ArchiveSentinel) /// sentinel
		+ sizeof(ArchiveFileHeader) /// header 
		+ sizeof(char)*labelSizeBytes /// label
		+ sizeof(ArchiveValue)*ArrayCount(archive.values) /// values
		+ sizeof(char)*ArrayCount(archive.dataHeap); /// data

	ArchiveFileHeader header = (ArchiveFileHeader) {0};

	size_t offset = 0;

	offset += sizeof(asd_ArchiveSentinel);

	header.version = asd_ARCHIVEVERSION;
	header.headerSizeBytes = sizeof(ArchiveFileHeader);
	offset += sizeof(ArchiveFileHeader);

	header.labelOffset = offset;
	header.labelSizeBytes = sizeof(char)*labelSizeBytes;
	offset += sizeof(char)*labelSizeBytes;

	header.valueTableOffset = offset;
	header.valueTableSizeBytes = sizeof(ArchiveValue)*ArrayCount(archive.values);
	offset += sizeof(ArchiveValue)*ArrayCount(archive.values);

	header.dataHeapOffset = offset;
	header.dataSizeBytes = sizeof(char)*ArrayCount(archive.dataHeap);

	/// obtain the memory block
	void *memory = allocator.alloc(memorySize, allocator.markup);

	/// obtain destination pointers into memory block
	void *memSentinel = asd_ArchiveMovePointerAddress(memory, 0);
	void *memHeader = asd_ArchiveMovePointerAddress(memory, sizeof(asd_ArchiveSentinel));
	void *memLabel = asd_ArchiveMovePointerAddress(memory, header.labelOffset);
	void *memValueTable = asd_ArchiveMovePointerAddress(memory, header.valueTableOffset);
	void *memDataHeap = asd_ArchiveMovePointerAddress(memory, header.dataHeapOffset);

	/// write sentinel
	for (int i = 0; i < sizeof(asd_ArchiveSentinel); i++)
	{ ((char*)memSentinel)[i] = asd_ArchiveSentinel[i]; }

	/// write header
	((ArchiveFileHeader*)memHeader) = header;

	/// write label
	for (int i = 0; i < labelSizeBytes; i++)
	{ ((char*)memLabel)[i] = label[i]; }

	/// write valueTable
	for (int i = 0; i < ArrayCount(archive.values); i++)
	{ ((ArchiveValue*)memValueTable)[i] = archive.values[i]; }

	/// write dataHeap
	for (int i = 0; i < ArrayCount(archive.dataHeap); i++)
	{ ((unsigned char*)memDataHeap)[i] = archive.dataHeap[i]; }

	return (asd_MemBlock){memory, memorySize};
}
#endif

/// tested: no
void ArchiveDestroy( Archive *archive)
{
	archive->allocator.free(archive->values);
	archive->allocator.free(archive->dataHeap);
	archive->allocator.free(archive->label);
	archive->allocator.free(archive->nested);
	*archive = {0};
}
/// tested: no
Archive ArchiveDeleteField( Archive archive, char *label)
{
	
}

/// tested: no
void ArchiveAddValue(Archive *io_archive, char *label, size_t labelSize, void *data, size_t dataSize, ArchiveValueType type);
{
	size_t labelOffset = ArchiveAddData(io_archive, label, labelSize);
	size_t dataOffset = ArchiveAddData(io_archive, data, dataSize);

	/// force null termination 
	io_archive->dataHeap[labelOffset + (labelSize-1)] = 0;
	
	ArchiveValue value = (ArchiveValue)
	{
		.type = type,
		.labelOffset = labelOffset,
		.labelSizeBytes = labelSize,
		.offset = dataOffset,
		.sizeBytes = dataSize,
	};

	/// add the value entry
	size_t valueSlot = ArrayReserve((void**)&io_archive->values, 1);
	io_archive->values[valueSlot] = value;
}

/// tested: no
void ArchiveAddInt( Archive *io_archive, char *label, int value)
{
	size_t labelSize = strlen(label);
	labelSize++;
	ArchiveAddValue(io_archive, label, labelSize, &value, sizeof(value), Archive_Int);
}

/// tested: no
void ArchiveAddFloat( Archive *io_archive, char *label, float value)
{
	size_t labelSize = strlen(label);
	labelSize++;
	ArchiveAddValue(io_archive, label, labelSize, &value, sizeof(value), Archive_Float);
}

/// tested: no
void ArchiveAddString( Archive *io_archive, char *label, char *value, size_t len)
{
	size_t labelSize = strlen(label);
	labelSize++;

	size_t stringSize = strlen(label);
	stringSize++;

	ArchiveAddValue(io_archive, label, labelSize, value, stringSize, Archive_String);
}

/// tested: no
/// DOC: This function 'moves' the archive pointed to by io_nested, and nulls
///  the instance pointed to by the argument. This is because it is expected that
///  archives will only ever live at one place in memory, so this nesting means
///  by definition the previous reference should be removed.
/// If C had unique pointers, I'd use one here.
void ArchiveAddNested( Archive *io_archive, char *label, Archive *io_nested)
{
	size_t labelSize = strlen(label);
	labelSize++;

	size_t nestedSlot = ArrayReserve((void**)&io_archive->nested, 1);
	io_archive->nested[nestedSlot] = *nested;

	*io_archive = (Archive) {0};

	ArchiveValue value = (ArchiveValue)
	{
		.type = Archive_Nested,
		.labelOffset = labelOffset,
		.labelSizeBytes = labelSize,
		.offset = index,
		.sizeBytes = 0,
	};

	size_t valueSlot = ArrayReserve((void**)&io_archive->values, 1);
	io_archive->values[valueSlot] = value;
}

/// tested: no
void ArchiveAddBinary( Archive *io_archive, char *label, void *value, size_t len)
{
	size_t labelSize = strlen(label);
	labelSize++;

	ArchiveAddValue(io_archive, label, labelSize, value, len, Archive_Binary);
}

/// tested: no
MAYBE(void_ptr) ArchiveGetValueDataReference(Archive *archive, char *label, ArchiveValueType correctType, size_t correctSize)
{
	MAYBE(ArchiveValue) fetch = ArchiveFetchValue(archive,label);
	if (!fetch.valid)
	{
		asd_LogError(__func__ " attempted to fetch value %s, but it was not found.", label); 
		return (MAYBE(void_ptr)) {false};
	}

	ArchiveValue value = fetch.item;

	if (value.type != correctType)
	{
		/// NOTE: duplicate warning in ArchiveGetNested
		asd_LogError(__func__ " expected to find value of type %s but instead found type %s.", 
			archiveValueTypeLabels[correctType],
			archiveValueTypeLabels[value.type]);
		return (MAYBE(void_ptr)) {false};
	}

	if (value.sizeBytes != correctSize)
	{
		asd_LogError(__func__ " found value of type %s but its size was %i instead of %i.", 
			archiveValueTypeLabels[correctType],
			value.sizeBytes, correctSize );
		return (MAYBE(void_ptr)) {false};
	}

	return (MAYBE(void_ptr)) {true, asd_ArchiveMovePointerAddress(archive.dataHeap, value.offset)};
}

/// tested: no
MAYBE(int) ArchiveGetInt(Archive *archive, char *label)
{
	ArchiveValueType correctType = Archive_Int;
	size_T correctSize = sizeof(int);

	MAYBE(void_ptr) data = ArchiveGetValueDataReference(archive, label, correctType, correctSize);
	MAYBE(int) value = {data.valid, 0};

	if (data.valid)
	{ value.item = *((int*)data.item); }

	return value;
}

/// tested: no
MAYBE(float) ArchiveGetFloat(Archive *archive, char *label)
{
	ArchiveValueType correctType = Archive_Float;
	size_T correctSize = sizeof(float);

	MAYBE(void_ptr) data = ArchiveGetValueDataReference(archive, label, correctType, correctSize);
	MAYBE(float) value = {data.valid, 0};

	if (data.valid)
	{ value.item = *((float*)data.item); }

	return value;
}

/// tested: no
MAYBE(char_ptr) ArchiveGetString(Archive *archive, char *label)
{
	ArchiveValueType correctType = Archive_String;
	size_T correctSize = sizeof(char*);

	MAYBE(void_ptr) data = ArchiveGetValueDataReference(archive, label, correctType, correctSize);
	MAYBE(char_ptr) value = {data.valid, 0};

	if (data.valid)
	{ value.item = (char*)data.item; }

	return value;
}

/// tested: no
MAYBE(void_ptr) ArchiveGetBinary(Archive *archive, char *label)
{
	ArchiveValueType correctType = Archive_Binary;
	size_T correctSize = sizeof(void *);

	MAYBE(void_ptr) data = ArchiveGetValueDataReference(archive, label, correctType, correctSize);
	MAYBE(void_ptr) value = {data.valid, 0};

	if (data.valid)
	{ value.item = (void *)data.item; }

	return value;
}

/// tested: no
MAYBE(archive_ptr) ArchiveGetNested(Archive *archive, char *label)
{
	MAYBE(ArchiveValue) fetch = ArchiveFetchValue(archive,label);
	if (!fetch.valid)
	{
		asd_LogError(__func__ " attempted to fetch value %s, but it was not found.", label); 
		return (MAYBE(archive_ptr)) {false};
	}

	ArchiveValue value = fetch.item;

	if (value.type != Archive_Nested)
	{
		/// NOTE: duplicate warning in ArchiveGetNested
		asd_LogError(__func__ " expected to find value of type %s but instead found type %s.", 
			archiveValueTypeLabels[Archive_Nested],
			archiveValueTypeLabels[value.type]);
		return (MAYBE(archive_ptr)) {false};
	}

	MAYBE(archive_ptr) result = {true, archive->nested+value.offset};
}

#endif // IMPLEMENTATION
