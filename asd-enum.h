/* 
 License at bottom.

 EXAMPLE USAGE:

	#define ENUM_NAME Op
	#define ENUM_MEMBERS\
		ENUM_MEMBER(add)\
		ENUM_MEMBER(sub)\
		ENUM_MEMBER(div)\
		ENUM_MEMBER(mul)

	#include "asd-enum.h"

	Op op = Op_add;
	char *label = Op_names[Op_add];
*/

#define JOIN2(a,b) a ## b
#define JOIN(a,b) JOIN2(a,b)
#define STRFY2(a) #a
#define STRFY(a) STRFY2(a)

typedef enum ENUM_NAME 
{
	#define ENUM_MEMBER(name) JOIN(ENUM_NAME, JOIN(_, name)),
	ENUM_MEMBERS
	#undef ENUM_MEMBER
	JOIN(ENUM_NAME, _count),
} ENUM_NAME;

char *JOIN(ENUM_NAME, _names)[] = 
{
	#define ENUM_MEMBER(name) STRFY(ENUM_NAME) "_" #name,
	ENUM_MEMBERS
	#undef ENUM_MEMBER
};

#undef STRFY2
#undef STRFY
#undef JOIN2
#undef JOIN
#undef ENUM_NAME
#undef ENUM_MEMBERS

/*
Copyright (c) 2022 Andrew Dunetz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
